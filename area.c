#ifndef AREA_H
#define AREA_H

int areaRect(int x, int y)
{
	return x * y;
}

double areaTriangle(int x, int y)
{
	return (x * y) / 2;
}

double areaCircle(int r)
{
	return PI * r * r;
}

#endif