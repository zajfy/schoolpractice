
#include <stdio.h>

double nfak(double n)
{

	double result=1;
	for(int i=1;i<=n;i++)
	{
		result *= i;
	}
	return result;
}

double bnk(double n, double k)
{
	return (nfak(n))/(nfak(k)*nfak(n-k));
}

int main()
{
	double n,k;
	printf("Enter 2 values. \n");
	scanf("%lf", &n);
	scanf("%lf", &k);
	printf("bnk: %.2f",bnk(n,k));
}