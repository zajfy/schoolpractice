
#include <stdio.h>
#include "weatherobsinc.h"

int main()
{
	int temp, hour, min;
	printf("Enter temp: ");
	scanf("%d", &temp);
	actual_temp(temp);
	printf("Enter hour: ");
	scanf("%d", &hour);
	obs_hour(hour);
	printf("Enter minute: ");
	scanf("%d", &min);
	obs_minute(min);
	observation(hour,min,temp);
}