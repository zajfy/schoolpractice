
#include <stdio.h>

void multiplication(int input)
{
	for(int i = 1; i < 11; i++)
	{
		printf("%d : %d\n",i , input*i);
	}
}

int main()
{
	int input;
	printf("Enter a number to print out the multiplication from: \n");
	do
	{
		scanf("%d", &input);
		if(input > 11 && input < 0)
		{
			printf("Wrong input\n");
		}
	
	}
	while(input > 11 && input < 0);	
	multiplication(input);
	
}