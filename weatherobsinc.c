
#include <stdio.h>

static int minute=0;
static int hour=0;
static int temperature=0;

int observation(int h, int m, int t)
{
	printf("%d:%d %dC",h,m,t);
}

void actual_temp(int t)
{
	temperature = t;
}

void obs_hour(int h)
{
	hour = h;
}

void obs_minute(int m)
{
	minute = m;
}