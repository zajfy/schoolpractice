//triangle 

#include <stdio.h>
#include <math.h>

#define PI 3.141592653589793

int main()
{
	double a,b,c,v,alpha;
	printf("Enter a: ");
	scanf("%lf",&a);
	printf("\nEnter b: ");
	scanf("%lf",&b);
	printf("\nEnter the angle of alpha: ");
	scanf("%lf",&v);
	alpha = v * 2 * PI / 360;
	c = sqrt(a * a + b * b - 2 * a * b * cos(alpha));
	printf("%f",c);
	
	if(fabs(a-b)<pow(10,-10) && fabs(a-c)<pow(10,-10))
	{
		printf("\nEquilateral");
	}
	else if(fabs(a-b)<pow(10,-10) || fabs(a-c)<pow(10,-10) || fabs(b-c)<pow(10,-10))
	{
		printf(" \nIsosceles");
	}
	else
	{
		printf("\nUnequal sided");
	}
		
}