
#include <stdio.h>

//get the median from the array, for both even and odd amount of numbers
double calcMedian(double result[], int length)
{

    if(length % 2 == 0)
    {
        return (result[length/2] + result[length/2 - 1]) / 2;
    }
    else
    {
        return result[length/2];
    }
}
//return min number
//use after sort
double calcMin(double result[])
{
    return result[0];
}

//return max number
//use after sort
double calcMax(double result[], int length)
{
    return result[length-1];
}

//return the mean number in the array
double calcMean(double result[], int length)
{
    double temp;
    for(int i = 0; i < length; i++)
    {
        temp += result[i];
    }
    //times 10 to bring the result up to percentage
    return ((temp / length) * 10);
}

//sort the array small to large
void resultSort(double result[], int length)
{
    for(int i = 0; i < length; i++)
    {
        for(int j = 0; j < length; j++)
        {
            if(result[i] > result[j])
            {
                double temp = result [i];
                result[i] = result[j];
                result[j] = temp;
            }
        }
    }
}

//input func
void inputFunc(double result[], int length)
{
    for(int i = 0;  i < length; i++)
    {
        printf("Enter result nr %d:",i+1);
        scanf("%lf", &result[i]);
    }
}
//need to get my 2d array in here somehow

void collectAndCalc(double result[], int length)
{
    inputFunc(result,length);
    resultSort(result,length);
    //ops
    //right now called as void but are double, trying to figure out a clean way to fix this
    calcMax(result, length);
    calcMean(result, length);
    calcMedian(result, length);
    calcMin(result);
}

int main()
{
    double stats[2][4];
    double resultsIoT[10];
    double resultsMjU[10];

    //should have for both arrays, but lets assume they are of the same length
    int length = sizeof(resultsIoT) / sizeof(resultsIoT[0]);

    printf("Test results for IoT\n");
    collectAndCalc(resultsIoT,length);
    printf("Test results for MjU\n");
    collectAndCalc(resultsMjU,length);

    //enter the results into the 2d array
    for(int i = 0; i < 2; i++)
    {
        for(int j = 0; j < length; j++)
        {
            switch(i)
            {
                case 0:
                    stats[i][j] = resultsMjU[j];
                    break;
                case 1:
                    stats[i][j] = resultsIoT[j];
                    break;
            }
        }
    }

    //print out the 2d array
    for(int i = 0; i < 4; i++)
    {
        switch(i)
        {
            case 0:
                printf("Mean:\t");
                break;
            case 1:
                printf("Median:\t");
                break;
            case 2:
                printf("Min:\t");
                break;
            case 3:
                printf("Max:\t");
                break;

        }
        for(int j = 0; j < 2; j++)
        {
            switch(j)
            {
                case 0:
                    printf("MjU: ");
                    break;
                case 1:
                    printf("IoT: ");
                    break;
            }

            printf("%.2lf",stats[j][i]);

            if(i == 0)
            {
                printf("%%");
            }
            printf("\t");
        }
        printf("\n");
    }
}