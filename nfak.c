// nfakulty
#include <stdio.h>

int nfak(double n)
{

	double result=1;
	for(int i=1;i<=n;i++)
	{
		result *= i;
	}
	return result;
}

int main()
{
	int faculty;
	printf("Mata in n som du vill beräkna n! för: ");
	scanf("%d",&faculty);
	printf("Fakulteten av %d är: %d",faculty,nfak(faculty));
}