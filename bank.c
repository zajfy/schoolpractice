
#include <stdio.h>

double deposit(double depo_sum, double capital)
{
	return capital + depo_sum;
}

double withdraw(double draw_sum, double capital)
{
	return capital - draw_sum;
	
}

double interest(double rate, double capital)
{
	rate = rate/100;
	capital *= rate;
}

void account(double capital)
{
	printf("Current account balance: %.2f", capital);
}

int main()
{
	double capital = 0.0,rate = 23.5,input;
	int choice;
	do
	{
		input = 0.0;
		printf("\n1.Check Account \n 2. Deposit money \n 3. Withdraw money \n 4. Interest \n 5. Quit\n");
		scanf("%d", &choice);
		switch(choice)
		
		{
			case 1:
				account(capital);
			break;
			case 2:
				printf("Enter amount to deposit: ");
				scanf("%f",	&input);
				capital = deposit(input, capital);
			break;
			case 3:
				printf("Enter amount to withdraw: ");
				scanf("%f", &input);
				capital = withdraw(input, capital);
			break;
			case 4:
				printf("Your money will increase with %.2f to %.2f from %.2f interest rate.",interest(rate,capital), interest(rate,capital)+capital,rate);
			break;
			case 5:
				printf("See ya later");
			break;
			default:
			break;
		}
	}while(choice != 5);	
}