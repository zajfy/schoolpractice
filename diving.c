/*
	Diving contest score calculator
*/
#include <stdio.h>
#include <stdlib.h>

int main()
{
	int judges,difficulty,score;
	int *points;
	
	//Judge check. If judges are less than 3 reenter amount of judges
	do
	{
		printf("Enter the amount of judges");
		if(scanf("%d",&judges) != 1)
		{
			exit(0);
		}
		if(judges<3)
		{
			printf("Too few judges, please get more.");
		}	
	}while(judges<3);
	printf("Please enter the difficulty of the dive");
	if(scanf("%d",&difficulty) != 1)
		{
			exit(0);
		}
	points = malloc(judges * sizeof(int));
	for(int i = 0; i < judges; i++)
	{
		printf("Enter points: ");
		if(scanf("%d",points[i]) != 1)
		{
			free(points);
			exit(0);
		}
	}
	
	//check entire array for highest and lowest amount and remove those
	for(int i = 0; i<judges-1;i++)
	{
		for(int j=1;j<judges;j++)
		{
			//if j is bigger than i, switch places. This places the biggest first and the smallest last in the array
			if(points[i]<points[j])
			{
				int temp = points[i];
				points[i]=points[j];
				points[j] = temp;
			}
		}
	}
	//add all points together for the last score calc
	for(int i=1; i<judges-1;i++)
	{
		score += points[i];
	}
	score /= judges-2;
	
	score = score * 3 * difficulty;
	printf("Score: %d", score);
	free(points);
}