

// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
  
}

// the loop function runs over and over again forever
void loop() {
  int toggle=1;
  for (int j = 0; j < 3; ++j)
        {
            for(int i=0;i<3;i++)
            {
                if(toggle==1)
                {
                    digitalWrite(LED_BUILTIN, HIGH);
                    delay(200);
                    digitalWrite(LED_BUILTIN, LOW);
                }
                if(toggle==-1)
                {
                    digitalWrite(LED_BUILTIN, HIGH);
                    delay(600);
                    digitalWrite(LED_BUILTIN, LOW);
                }
            }
            toggle *= -1;
            delay(200);
        }
        toggle=1;                   // wait for a second
        delay(800);
}
