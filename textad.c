#include <stdio.h>
#include <stdlib.h>

struct option {
	int page;	// Gå till sida när valet väljs
	int keymask;	// Kräv nyckel för att låsa upp alternativ
	char* text;	// text
};
struct page {
	char* text;	// text
	int count;	// Antal val
	int keymask;	// Ge nyckel när sidan visas.
	struct option options[5];
};


/* Keymask: (Binärt!)
 * 0 = kräv/ge ingen nyckel
 * 1 = Nyckel 1
 * 2 = Nyckel 2
 * 3 = Nyckel 1 & 2
 * etc...
 */


struct page pages[] =
{
	{	// sida 0
		"Det här är sida 0.", // story
		3, // Antal val
		0, // nyckel
		{
			{1,0,"gå till sida 1"}, // val 1
			{0,0,"gå till sida 0"}, // val 2
			{2,1,"Detta val kräver nyckeln."}, // val 3
		}
	},
	{	// sida 1
		"Det här är sida 1. Du har nu nyckel #1.",
		1, // antal val
		1, // nyckel
		{
			{0,0,"Gå till sida 0"} // val 1
		}
	},
	{
		// sida 2 - game over
		"Grattis!!!! Skriv 0 för att avsluta.",
		0,
		0,
		{
		}
	}
};

int main()
{
	int keymask=0;
	int page=0; // Första sidan

	int avail_opt[5];

	struct page *p;
	while(1)
	{
		p = &pages[page];

		// Lägg till nycklar om det är skriptat
		keymask |= p->keymask;
		// Visa sidtext
		printf("Text: %s\n",p->text);

		// räkna ut alla tillgängliga val
		int avail = 0;
		for(int i=0;i<p->count;i++)
		{
			int km = p->options[i].keymask;
			// Finns alla nycklar som vi kräver för det här rummet...
			//printf("Debug alt %d km=%d, nu har vi %d\n",i,km,keymask);
			if((keymask & km) == km)
			{
				avail_opt[avail++] = i;
			}
		}


		// Visa alla val
		for(int i=0;i<avail;i++)
		{
			printf("Val %d: %s\n",i+1,p->options[avail_opt[i]].text);
		}
		int val;
		scanf("%d",&val);
		printf("Du valde %d...\n",val);
		// 0 stänger programmet
		if(val==0)
			return 0;
		// Annars gå till den länkade sidan
		else if(val<=avail)
		{
			page = p->options[avail_opt[val-1]].page;
		}
	}

	return 0;
}

