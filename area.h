

#define PI 3.14159265358979323

int areaRect(int x, int y);
double areaCircle(int r);
double areaTriangle(int x, int y);