
#ifndef FIBOFUNC_H
#define FIBOFUNC_H
#include <stdio.h>
#include "fiboFunc.h"
void fiboFunc()
{
	static int first = 0;
	static int second = 1;	
	
	int temp = first + second;
	first = second;
	second = temp;
	printFibo(temp);
}

void printFibo(int print)
{
	printf("%d : ",print);
}
#endif