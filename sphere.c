//Volume and area of a sphere

#include <stdio.h>
#include <math.h>

#define PI 3.141592

int main()
{
	float v,r,a;
	printf("Enter the radious of the sphere:\n");
	scanf("%f",&r);
	v=(4*PI*pow(r,3))/3;
	a=4*PI*pow(r,2);
	printf("The volume of the sphere is: %f and the area of the sphere is: %f\n",v,a);
	
	system("pause");
}