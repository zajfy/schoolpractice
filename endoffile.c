/*

End with EOF

*/

#include <stdio.h>

int main()
{
	int input, big = 0, small = 10000;
	while(1)
	{
		printf("\nAnge ett heltal, markera EOF för att sluta: ");
		if(scanf("%d",&input) != 1)
		{
			break;
		}
		if(input > big)
		{
			big = input;
		}
		if(input < small)
		{
			small = input;
		}
		
	}
	printf("\nStörsta inmatning: %d\n Minsta inmatning: %d", big, small);
}