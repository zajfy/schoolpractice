//circle extension from practice 2.5

#include <stdio.h>

#define PI 3.141592653589793

int main()
{
	double rad,area,circumference;
	printf("Enter the circles radius: ");
	scanf("%f",&rad);
	if(rad>0)
	{
		area = PI * rad * rad;										//HOW DOES IT BECOME 0.0
		circumference = 2 * PI * rad;								//HOW DOES IT BECOME 0.0
		printf("The area is %f\n",area);
		printf("The circumference is %f\n",circumference);
	}
	else
	{
		printf("Error, wrong input");
	}
	
}